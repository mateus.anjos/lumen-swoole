<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDO;

class TestController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function pingHandler(Request $request): JsonResponse
    {
        return new JsonResponse(['ack' => time()]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function healthCheckEloquentConnection(Request $request): JsonResponse
    {
        \Log::debug(time());
        return new JsonResponse(
            [
                'doctrine_is_successfully_connect' => DB::connection()->getPdo()->getAttribute(
                    PDO::ATTR_CONNECTION_STATUS
                )
            ]
        );
    }
}
