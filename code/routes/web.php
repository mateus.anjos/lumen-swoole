<?php
use Laravel\Lumen\Routing\Router;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Router $router */

$router->get('ping', [
    'as' => 'ping', 'uses' => 'TestController@pingHandler'
]);
$router->get('health-check', [
    'as' => 'health-check', 'uses' => 'TestController@healthCheckEloquentConnection'
]);
