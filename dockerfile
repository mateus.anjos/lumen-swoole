FROM php:7.4-fpm

LABEL maintainer = 'mateus.anjos@finnet.com.br'

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libreadline-dev \
        libpng-dev \
        openssl libssh-dev \
        libnghttp2-dev \
        libonig-dev \
        libhiredis-dev \
        libcurl4-openssl-dev \
        vim openssl libssl-dev wget git -y \
        software-properties-common -y  \
        libpq-dev -y  \
        git -y  \
    && docker-php-ext-install iconv pdo_mysql mysqli mbstring json opcache curl pgsql pdo_pgsql -j$(nproc) sockets \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd

RUN pecl install mcrypt redis && docker-php-ext-enable mcrypt redis

# WARNING: Disable opcache if you run you php
RUN echo "opcache.enable_cli=0" >>  /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

RUN git clone https://github.com/swoole/swoole-src.git \
  && cd swoole-src \
  && phpize \
  && ./configure --enable-http2 --enable-sockets --enable-openssl \
  && make && make install \
  && docker-php-ext-enable swoole \
  && rm -rf /tmp/*

RUN echo "log_errors = On" >> /usr/local/etc/php/conf.d/log.ini \
    && echo "error_log=/dev/stderr" >> /usr/local/etc/php/conf.d/log.ini

RUN curl -s https://getcomposer.org/installer | php

RUN mv composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

RUN mkdir -p /app

WORKDIR /app

COPY ./code .

RUN composer install

EXPOSE 8080
